$(document).ready(function(){
  svg4everybody();

  $("#promo-slider").owlCarousel({
    items: 1
  });

  $("#promo-partners").owlCarousel({
    loop: true,
    dots: false,
    autoplay: true,
    smartSpeed: 750,
    margin: 30,
    responsive: {
      0: {
        items: 2
      },
      768: {
        items: 3
      },
      992: {
        items: 4
      },
      1200: {
        items: 5
      }
    }
  });

  $("#project-gallery").owlCarousel({
    dots: false,
    autoplay: true,
    smartSpeed: 750,
    margin: 2,
    nav: true,
    navText: ['<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>', '<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>'],
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 3
      },
      992: {
        items: 4
      },
      1920: {
        items: 5
      }
    }
  });

  $('.s-project-info__gallery-slider').lightGallery({
    selector: '.s-project-info__gallery-item',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });

});